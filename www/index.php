<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey;

use nextdev\Geoffrey\Geoffrey;

require __DIR__ . "/../bootstrap.php";

$configFile = \getenv('GEOFFREY_CONFIG')?: __DIR__ . "/../.etc/config.json";

$config = (\file_exists($configFile)? \json_decode(\file_get_contents($configFile), true): []);

\header("HTTP/1.1 500");

if (!\is_array($config)) {
    echo "Syntax error in configuration file";
    exit;
}

$geoffrey = new Geoffrey($config);

$geoffrey->debug = (int) \getenv('GEOFFREY_DEBUG');

$geoffrey->run();

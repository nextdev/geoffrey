<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\Cache;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use SplObjectStorage;

use function GuzzleHttp\Psr7\parse_response;
use function GuzzleHttp\Psr7\str;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Client\NetworkExceptionInterface;
use Psr\Http\Client\RequestExceptionInterface;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * @internal This class is not covered by the backward compatibility promise
 */
class Cache
{
    const CACHEABLE_METHODS = ["GET", "HEAD"];
    const CACHEABLE_STATUS = [200, 203, 204, 206, 300, 301, 404, 405, 410, 414, 501];
    const PREFIX_RESPONSE = "res";
    const HASH_ALGO = "sha1";

    /**
     * @var int|DateInterval|null
     */
    public $cacheMax = 24*60*60;

    /**
     * @var int|DateInterval|null
     */
    public $cacheStale = 24*60*60;

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var CacheInterface
     */
    protected $driver;

    /**
     * @var ResponseFactoryInterface
     */
    protected $responseFactory;

    /**
     * @var StreamFactoryInterface
     */
    protected $streamFactory;

    /**
     * @param CacheInterface $driver
     *  Storage driver for caching
     * @param ClientInterface $client
     *  PSR-16 compatible client for forwarding requests
     * @param ResponseFactoryInterface $responseFactory
     *  PSR-17 compatible factory for creating reponses on network errors
     * @param StreamFactoryInterface $streamFactory
     *  PSR-17 compatible factory
     */
    public function __construct(
        CacheInterface $driver,
        ClientInterface $client,
        ResponseFactoryInterface $responseFactory,
        StreamFactoryInterface $streamFactory
    ) {
        $this->client = $client;
        $this->driver = $driver;
        $this->responseFactory = $responseFactory;
        $this->streamFactory = $streamFactory;
    }

    /**
     * Clean up expired entries
     *
     * Forwards the call to the driver if an "evict" method exists, so that the driver can take all actions required
     * to make the underlying implementations get rid of obsolete entries.
     */
    public function evict(): void
    {
        if (\method_exists($this->driver, 'evict')) {
            $this->driver->evict();
        }
    }

    /**
     * Handle a request
     *
     * Use a cached response if the requests can be satisfied by it.
     * Otherwise forward the request.
     *
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     */
    public function sendRequest(
        RequestInterface $request
    ): ResponseInterface {
        if (!$this->isCacheableRequest($request, $requestAttr)) {
            return $this->doSendRequest($request, $requestAttr, $responseAttr);
        }

        $requestAttr['plainKey'] = $this->getRequestCacheKey($request);

        $response = $this->getCacheEntry($requestAttr['plainKey'], $request, $requestAttr, $responseAttr, $vary);

        if (!isset($response) && isset($vary)) {
            $requestAttr['varyKey'] = $this->getRequestCacheKey($request, $vary);
            $response = $this->getCacheEntry($requestAttr['varyKey'], $request, $requestAttr, $responseAttr, $vary);
        }

        return isset($response)? $response: $this->doSendRequest($request, $requestAttr, $responseAttr);
    }

    /**
     * Initialize the cache
     *
     * Forwards the call to the driver if a "setup" method exists, so that the driver can take all actions required
     * to make the underlying implementations ready to go (like setting up database tables).
     */
    public function setup(): void
    {
        if (\method_exists($this->driver, 'setup')) {
            $this->driver->setup();
        }
    }

    protected function doSendRequest(
        RequestInterface $request,
        ?array &$requestAttr,
        ?array &$responseAttr,
        int $gatewayErrorStatus = 502
    ): ResponseInterface {
        try {
            $responseAttr['requestTime'] = new DateTimeImmutable();
            $response = $this->client->sendRequest($request);

            if (isset($requestAttr['plainKey'])
                && $this->isCacheableResponse($request, $requestAttr, $response, $responseAttr)
            ) {
                $this->setCacheEntry($request, $requestAttr, $response, $responseAttr);
            }
        } catch (NetworkExceptionInterface $e) {
        } catch (RequestExceptionInterface $e) {
        }
        if (isset($e)) {
            $response = $this->responseFactory->createResponse($gatewayErrorStatus);
            $response = $response->withBody($this->streamFactory->createStream(\sprintf(
                "Requesting resource %s failed:\n%s",
                (string) $request->getUri(),
                $e->getMessage()
            )));
            $responseAttr = [];
        }
        return $response;
    }

    protected function getCacheEntry(
        string $key,
        RequestInterface $request,
        ?array &$requestAttr,
        ?array &$responseAttr,
        ?array &$vary
    ): ?ResponseInterface {
        $data = $this->driver->get($key);
        $data = ($data != "")? @\unserialize($data): null;
        if ($data === false) {
            return null;
        }

        if (isset($data['responseAttr'])) {
            $responseAttr = (array) $data['responseAttr'];
        }

        if (isset($data['vary'])) {
            $vary = (array) $data['vary'];
        }

        if (isset($data['response'])) {
            $response = parse_response($data['response']);
            return $this->validateResponse($request, $requestAttr, $response, $responseAttr);
        }
        return null;
    }

    protected function getHeaderDirectives(
        MessageInterface $message,
        string $name
    ): array {
        $ret = [];

        $h = $message->getHeader($name);
        foreach ($h as $v) {
            $ret = \array_merge($ret, \array_map(function ($v) {
                return \strtolower(trim($v));
            }, \explode(",", $v)));
        }

        return $ret;
    }

    protected function getMessageCacheControl(
        MessageInterface $message,
        ?array &$messageAttr
    ): array {
        if (isset($messageAttr['cacheControl'])) {
            return $messageAttr['cacheControl'];
        }

        $messageAttr['cacheControl'] = [];

        $headerCacheControl = $this->getHeaderDirectives($message, "Cache-Control");

        if (\count($headerCacheControl) === 0 || ($message instanceof RequestInterface)) {
            $pragma = $this->getHeaderDirectives($message, "Pragma");
            if (\in_array("no-cache", $pragma)) {
                $messageAttr['cacheControl']['no-cache'] = true;
            }
        }

        foreach ($headerCacheControl as $d) {
            if (\strpos($d, '=') !== false) {
                $d = \explode('=', $d, 2);
                /**
                 * When there is more than one value present for a given directive
                 * (e.g., two Expires header fields, multiple Cache-Control: max-age
                 * directives), the directive's value is considered invalid.
                 * @see https://tools.ietf.org/html/rfc7234#section-4.2.1
                 */
                if (isset($messageAttr['cacheControl'][$d[0]])) {
                    $messageAttr['cacheControl'][$d[0]] = false;
                } else {
                    $messageAttr['cacheControl'][$d[0]] = (int) \trim($d[1], "\"'");
                }
            } else {
                $messageAttr['cacheControl'][$d] = true;
            }
        }

        $messageAttr['cacheControl'] = \array_filter($messageAttr['cacheControl'], function ($v) {
            return $v !== false;
        });

        return $messageAttr['cacheControl'];
    }

    protected function getRequestCacheKey(
        RequestInterface $request,
        array $varyKeys = []
    ): string {
        $method = $request->getMethod();

        $location = "\n" . $request->getUri()->withFragment("");

        $vary = "";
        foreach ($varyKeys as $k) {
            $v = $request->getHeader($k);
            $vary .= "\n" . \implode("\n", \array_map(function ($v) use ($k) {
                return $k . ":" . $v;
            }, \count($v) > 0? $v: [""]));
        }

        $k = "Range";
        $v = $request->getHeader($k);
        $range = "\n" . \implode("\n", \array_map(function ($v) use ($k) {
            return $k . ":" . $v;
        }, \count($v) > 0? $v: [""]));

        return static::PREFIX_RESPONSE . \hash(static::HASH_ALGO, $method . $location . $vary . $range, true);
    }

    protected function getResponseDate(
        ResponseInterface $response,
        ?array &$responseAttr
    ): DateTimeImmutable {

        $date = $this->getResponseTimestamp("Date", $response, $responseAttr);
        if (isset($date)) {
            return $date;
        }

        $ageValue = $response->getHeader("Age");
        $ageValue = (\count($age) > 1)? \max(...$age): $age[0] ?? null;
        if (isset($ageValue) && isset($responseAttr['requestTime'])) {
            return $this->addTime($responseAttr['requestTime'], -$ageValue);
        }

        return $responseAttr['requestTime'];
    }

    protected function getResponseExplicitFreshBefore(
        ResponseInterface $response,
        ?array &$responseAttr
    ): ?DateTimeImmutable {

        $cacheControl = $this->getMessageCacheControl($response, $responseAttr);
        if (isset($cacheControl['s-maxage']) && \is_int($cacheControl['s-maxage'])) {
            return $this->addTime($this->getResponseDate($response, $responseAttr), $cacheControl['s-maxage']);
        } elseif (isset($cacheControl['max-age']) && \is_int($cacheControl['max-age'])) {
            return $this->addTime($this->getResponseDate($response, $responseAttr), $cacheControl['max-age']);
        }

        $expires = $this->getResponseTimestamp('Expires', $response, $responseAttr);
        if (isset($expires)) {
            return $expires;
        }

        return null;
    }

    protected function getResponseHeuristicFreshBefore(
        RequestInterface $request,
        ?array &$requestAttr,
        ResponseInterface $response,
        ?array &$responseAttr
    ): ?DateTimeImmutable {
        /**
         * Note: Section 13.9 of [RFC2616] prohibited caches from calculating
         * heuristic freshness for URIs with query components
         */
        if ($request->getUri()->getQuery() !== "") {
            return null;
        }

        $lastModified = $this->getResponseTimestamp('Last-Modified', $response, $responseAttr);
        if (isset($lastModified)) {
            $date = $this->getResponseDate($response, $responseAttr);
            return $this->addTime($date, ($date->getTimestamp() - $lastModified->getTimestamp()) / 10);
        }

        return null;
    }

    protected function getResponseTimestamp(
        string $key,
        ResponseInterface $response,
        ?array &$responseAttr
    ): ?DateTimeImmutable {
        $key = \strtolower($key);

        if (!isset($responseAttr['timestamp'][$key])) {
            $header = $response->getHeader($key);
            if (\count($header) === 1) {
                $responseAttr['timestamp'][$key] = new DateTimeImmutable($header[0]);
            } else {
                $responseAttr['timestamp'][$key] = false;
            }
        }

        return ($responseAttr['timestamp'][$key] instanceof DateTimeImmutable)? $responseAttr['timestamp'][$key]: null;
    }

    protected function getResponseVary(
        ResponseInterface $response,
        ?array &$responseAttr
    ): ?array {
        if (isset($responseAttr['vary'])) {
            return $responseAttr['vary'];
        }

        $vary = $this->getHeaderDirectives($response, "Vary");

        $responseAttr['vary'] = $vary;

        return $vary;
    }

    /**
     * Determine if responses to this request might be cacheable
     */
    protected function isCacheableRequest(
        RequestInterface $request,
        ?array &$requestAttr
    ): bool {
        if (!\in_array($request->getMethod(), static::CACHEABLE_METHODS)) {
            return false;
        }

        $cacheControl = $this->getMessageCacheControl($request, $requestAttr);

        if (isset($cacheControl['no-store'])) {
            return false;
        }

        return true;
    }

    /**
     * Determine if a response may be cached and might be useful
     *
     * @see https://tools.ietf.org/html/rfc7234#section-3
     */
    protected function isCacheableResponse(
        RequestInterface $request,
        ?array &$requestAttr,
        ResponseInterface $response,
        ?array &$responseAttr
    ): bool {
        $vary = $this->getResponseVary($response, $responseAttr);

        /**
         * A Vary header field-value of "*" always fails to match.
         * @see https://tools.ietf.org/html/rfc7234#section-4.1
         */
        if (\in_array("*", $vary)) {
            return false;
        }

        $cacheControl = $this->getMessageCacheControl($response, $responseAttr);

        if (isset($cacheControl['no-store'])) {
            return false;
        } elseif (isset($cacheControl['private'])) {
            return false;
        } elseif ($response->hasHeader("Authorization")
            && !(isset($cacheControl['public'])
                || isset($cacheControl['must-revalidate'])
                || isset($cacheControl['s-maxage'])
            )
        ) {
            return false;
        }

        if (isset($cacheControl['public'])) {
            return true;
        } elseif (isset($cacheControl['max-age'])
            || isset($cacheControl['s-maxage'])
            || $response->hasHeader("Expires")
        ) {
            return true;
        } elseif (\in_array($response->getStatusCode(), static::CACHEABLE_STATUS)) {
            return true;
        }

        return false;
    }

    protected function mustRevalidate(
        RequestInterface $request,
        ?array &$requestAttr,
        ResponseInterface $response,
        ?array &$responseAttr,
        ?int &$warning
    ): bool {
        $requestCacheControl = $this->getMessageCacheControl($request, $requestAttr);
        $responseCacheControl = $this->getMessageCacheControl($response, $responseAttr);

        if (isset($requestCacheControl['no-cache'])
            || isset($responseCacheControl['no-cache'])
            || isset($responseCacheControl['must-revalidate'])
            || isset($responseCacheControl['proxy-revalidate'])
        ) {
            return true;
        }

        $now = new DateTimeImmutable;
        $responseDate = $this->getResponseDate($response, $responseAttr);
        $freshBefore = $this->getResponseExplicitFreshBefore($response, $responseAttr);
        if (!isset($freshBefore)) {
            $freshBefore = $this->getResponseHeuristicFreshBefore($request, $requestAttr, $response, $responseAttr);
            /**
             * @see https://tools.ietf.org/html/rfc7234#section-5.5.4
             */
            if (isset($freshBefore)
                && $freshBefore > $this->addTime($responseDate, 24*60*60)
                && $now > $this->addTime($responseDate, 24*60*60)
            ) {
                $warning = 113;
            }
        }
        $isFresh = isset($freshBefore) && ($freshBefore > $now);

        if (!$isFresh && !isset($requestCacheControl['max-stale'])) {
            return true;
        }

        if (isset($requestCacheControl['max-age'])) {
            if (!$isFresh && !isset($requestCacheControl['max-stale'])
                || $now > $this->addTime($responseDate, $requestCacheControl['max-age'])
            ) {
                return true;
            }
        }
        if (isset($requestCacheControl['max-stale'])) {
            if (!$isFresh && $now > $this->addTime($freshBefore, $requestCacheControl['max-stale'])) {
                return true;
            }
        }
        if (isset($requestCacheControl['min-fresh'])) {
            if (!$isFresh || $now > $this->addTime($freshBefore, -$requestCacheControl['min-fresh'])) {
                return true;
            }
        }

        if (isset($responseCacheControl['s-maxage'])) {
            if ($now > $this->addTime($responseDate, $responseCacheControl['s-maxage'])) {
                return true;
            }
        } elseif (isset($responseCacheControl['max-age'])) {
            if ($now > $this->addTime($responseDate, $responseCacheControl['max-age'])) {
                return true;
            }
        }

        return false;
    }

    protected function setCacheEntry(
        RequestInterface $request,
        array $requestAttr,
        ResponseInterface $response,
        ?array &$responseAttr
    ): void {


        $now = new DateTimeImmutable;
        $date = $this->getResponseDate($response, $responseAttr);
        $freshBefore = $this->getResponseExplicitFreshBefore($response, $responseAttr) ??
            $this->getResponseHeuristicFreshBefore($request, $requestAttr, $response, $responseAttr) ??
            $date;

        $ttl = $freshBefore->getTimestamp() - $now->getTimestamp();
        if (isset($this->cacheStale)) {
            $ttl += ($this->cacheStale instanceof DateInterval)?
                $freshBefore->add($this->cacheStale)->getTimestamp() - $freshBefore->getTimestamp():
                (int) $this->cacheStale;
        }
        if (isset($this->cacheMax)) {
            $ttl = \min(
                $ttl,
                ($this->cacheMax instanceof DateInterval)?
                    $now->add($this->cacheMax)->getTimestamp():
                    (int) $this->cacheMax
            );
        }

        if ($ttl <= 0) {
            return;
        }

        $plainData = \serialize([
            'response' => str($response),
            'responseAttr' => $responseAttr,
        ]);

        // str() reads the response body
        // rewind it so that the resulting response is in the same state whether cached or not
        $response->getBody()->rewind();

        $vary = $this->getResponseVary($response, $responseAttr);
        if (\count($vary) > 0) {
            $requestAttr['varyKey'] = $this->getRequestCacheKey($request, $vary);
            $varyData = \serialize(['vary' => $vary]);
            $this->driver->set($requestAttr['varyKey'], $plainData, $ttl);
            $this->driver->set($requestAttr['plainKey'], $varyData, $ttl);
        } else {
            $this->driver->set($requestAttr['plainKey'], $plainData, $ttl);
        }
    }

    /**
     * Validate that a cached response may be used as a response to the given request
     *
     * If revalidation is necessary, perform it and if applicable update cache status or cache a new response
     */
    protected function validateResponse(
        RequestInterface $request,
        ?array &$requestAttr,
        ResponseInterface $response,
        ?array &$responseAttr
    ): ?ResponseInterface {
        $mustRevalidate = $this->mustRevalidate($request, $requestAttr, $response, $responseAttr, $warning);

        if (!$mustRevalidate) {
            if ($warning === 113) {
                $response = $response->withAddedHeader("Warning", "113 - \"Heuristic Expiration\"");
            }
            if ($request->hasHeader("If-None-Match") && $response->hasHeader("ETag")
                && $request->getHeader("If-None-Match")[0] === $response->getHeader("Etag")[0]
                || $request->hasHeader("If-Modified-Since")
                    && new DateTimeImmutable($request->getHeader("If-Modified-Since")[0])
                        > $this->getResponseTimestamp("Last-Modified", $response, $responseAttr)
                        ?? $this->getResponseDate($response, $responseAttr)
            ) {
                return $response->withStatusCode(304)->withBody($this->streamFactory->createStream(""));
            } else {
                return $response;
            }
        }

        /*
        if ($response->hasHeader("ETag")) {
            $request = $request->withHeader("If-None-Match", $response->getHeader("ETag")[0]);
        } else {
            $request = $request->withHeader(
                "If-Modified-Since",
                $this->getResponseDate($response, $responseAttr)->format(DateTimeInterface::RFC850)
            );
        }
        */

        return $this->doSendRequest($request, $requestAttr, $newResponseAttr);
    }

    private function addTime(
        DateTimeImmutable $time0,
        int $diff
    ): DateTimeImmutable {
        return $time0->setTimestamp($time0->getTimestamp());
    }
}

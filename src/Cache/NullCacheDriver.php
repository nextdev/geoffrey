<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\Cache;

use Psr\SimpleCache\CacheInterface;

class NullCacheDriver implements CacheInterface
{
    public function clear(): bool
    {
        return true;
    }

    public function delete(
        $key
    ): bool {
        return true;
    }

    public function deleteMultiple(
        $keys
    ): bool {
        return true;
    }

    public function get(
        $key,
        $default = null
    ) {
        return $default;
    }

    public function getMultiple(
        $keys,
        $default = null
    ): iterable {
        $ret = [];

        if (\is_iterable($keys)) {
            foreach ($keys as $k) {
                $ret[$k] = $default;
            }
        }

        return $ret;
    }

    public function has(
        $key
    ): bool {
        return false;
    }

    public function set(
        $key,
        $value,
        $ttl = null
    ): bool {
        return true;
    }

    public function setMultiple(
        $values,
        $ttl = null
    ): bool {
        return true;
    }
}

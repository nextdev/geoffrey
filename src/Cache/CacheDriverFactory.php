<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\Cache;

use mysqli;

use Psr\SimpleCache\CacheInterface;

use nextdev\Geoffrey\Cache\MysqliCacheDriver;
use nextdev\Geoffrey\Cache\NullCacheDriver;

class CacheDriverFactory
{
    public static function createCacheDriver(
        $client,
        $config
    ): CacheInterface {
        if ($client instanceof mysqli) {
            $driver = new MysqliCacheDriver($client, $config['table'] ?? "cache");
            if (isset($config['maxSize']) && \is_numeric($config['maxSize'])) {
                $driver->maxValueLength = (int) $config['maxSize'];
            }
            return $driver;
        } else {
            return new NullCacheDriver();
        }
    }
}

<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\Cache;

use DateInterval;
use DateTime;
use mysqli;

use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

use nextdev\Geoffrey\Driver\MysqliDriverAbstract;

class MysqliCacheDriver extends MysqliDriverAbstract implements CacheInterface
{
    public $maxKeyLength = 43;
    public $maxValueLength = 0b1000000000000000000000000;

    public function clear(): bool
    {
        return $this->mysqli->query(\sprintf(
            'DELETE FROM %s',
            $this->table($this->tableName)
        ));
    }

    public function delete(
        $key
    ): bool {
        return $this->mysqli->query(\sprintf(
            'DELETE FROM %s WHERE %s=%s',
            $this->table($this->tableName),
            $this->field('key'),
            $this->value($key)
        ));
    }

    public function deleteMultiple(
        $keys
    ): bool {
        $qry = [];

        if (\is_iterable($keys)) {
            foreach ($keys as $k) {
                $qry[$k] = \sprintf(
                    'DELETE FROM %s WHERE %s=%s',
                    $this->table($this->tableName),
                    $this->field('key'),
                    $this->value($key)
                );
            }
        } else {
            throw new InvalidArgumentException("Argument 1 must be iterable");
        }

        $success = true;

        if (\count($qry) > 0 && $this->mysqli->multi_query(\implode(";", $qry))) {
            do {
                $success = $success && $this->mysqli->store_result();
            } while ($this->mysqli->more_results() && $this->mysqli->next_result());
        }

        return $success;
    }

    /**
     * Clean up expired entries
     */
    public function evict(): void
    {
        $this->mysqli->query(\sprintf(
            "DELETE FROM %s WHERE %s < NOW()",
            $this->table($this->tableName),
            $this->field('expires')
        ));
    }

    public function get(
        $key,
        $default = null
    ) {
        $result = $this->mysqli->query(\sprintf(
            'SELECT %s FROM %s WHERE %s=%s AND (%s IS NULL OR %5$s > NOW())',
            $this->field('value'),
            $this->table($this->tableName),
            $this->field('key'),
            $this->value($key),
            $this->field('expires')
        ));

        if ($result && ($r = $result->fetch_row())) {
            return $r[0];
        } else {
            return $default;
        }
    }

    public function getMultiple(
        $keys,
        $default = null
    ): iterable {
        $qry = [];

        if (\is_iterable($keys)) {
            foreach ($keys as $k) {
                $qry[$k] = \sprintf(
                    'SELECT %s FROM %s WHERE %s=%s AND (%s IS NULL OR %5$s > NOW())',
                    $this->field('value'),
                    $this->table($this->tableName),
                    $this->field('key'),
                    $this->value($key),
                    $this->field('expires')
                );
            }
        } else {
            throw new InvalidArgumentException("Argument 1 must be iterable");
        }

        $results = [];

        if (\count($qry) > 0 && $this->mysqli->multi_query(\implode(";", $qry))) {
            do {
                $result = $this->mysqli->store_result();
                
                if ($result && ($r = $result->fetch_row())) {
                    $results[$k] = $r[0];
                } else {
                    $results[$k] = $default;
                }
            } while ($this->mysqli->more_results() && $this->mysqli->next_result());
        }

        return \array_combine($keys, \array_pad($results, \count($keys), $default));
    }

    public function has(
        $key
    ): bool {
        $result = $this->mysqli->query(\sprintf(
            'SELECT 1 FROM %s WHERE %s=%s AND (%s IS NULL OR %4$s > NOW())',
            $this->table($this->tableName),
            $this->field('key'),
            $this->value($key),
            $this->field('expires')
        ));

        return $result? $result->fetch_row()[0] == 1: false;
    }

    public function set(
        $key,
        $value,
        $ttl = null
    ): bool {
        $expires = $this->expires($ttl);

        $key = (string) $key;
        $value = (string) $value;
        if (\strlen($key) > $this->maxKeyLength || \strlen($value) >= $this->maxValueLength) {
            return false;
        }

        return $this->mysqli->query($s = \sprintf(
            'REPLACE %s SET %s=%s,%s=%s,%s=%s',
            $this->table($this->tableName),
            $this->field('key'),
            $this->value($key),
            $this->field('expires'),
            $this->value($expires),
            $this->field('value'),
            $this->value($value)
        ));
    }

    public function setMultiple(
        $values,
        $ttl = null
    ): bool {
        $qry = [];
        $expires = $this->expires($ttl);

        if (\is_iterable($values)) {
            foreach ($values as $key => $value) {
                $key = (string) $key;
                $value = (string) $value;
                if (\strlen($key) > $this->maxKeyLength || \strlen($value) >= $this->maxValueLength) {
                    return false;
                }
                $qry[$k] = \sprintf(
                    'REPLACE %s SET %s=%s,%s=%s,%s=%s',
                    $this->table($this->tableName),
                    $this->field('key'),
                    $this->value($key),
                    $this->field('expires'),
                    $this->value($expires),
                    $this->field('value'),
                    $this->value($value)
                );
            }
        } else {
            throw new InvalidArgumentException("Argument 1 must be iterable");
        }

        $success = true;

        if (\count($qry) > 0 && $this->mysqli->multi_query(\implode(";", $qry))) {
            do {
                $success = $success && $this->mysqli->store_result();
            } while ($this->mysqli->more_results() && $this->mysqli->next_result());
        }

        return $success;
    }

    /**
     * Initialize the cache storage
     */
    public function setup(): void
    {
        $this->mysqli->query(\sprintf(
            "DROP TABLE IF EXISTS %s",
            "`" . $this->mysqli->real_escape_string($this->tableName) . "`"
        ));
        $this->mysqli->query(\sprintf(
            "CREATE TABLE IF NOT EXISTS %s (%s, %s, %s, %s)",
            "`" . $this->mysqli->real_escape_string($this->tableName) . "`",
            \sprintf("`key` VARBINARY(%d)", $this->maxKeyLength),
            "`expires` TIMESTAMP NULL",
            \sprintf("`value` BLOB(%d)", $this->maxValueLength),
            "PRIMARY KEY (`key`)",
            "INDEX(`expires`)"
        ));
    }
}

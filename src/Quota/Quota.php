<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\Quota;

use nextdev\Geoffrey\Quota\QuotaDriverInterface;
use nextdev\Geoffrey\Quota\ServiceUnavailableException;
use nextdev\Geoffrey\Quota\TooManyRequestsException;

/**
 * @internal This class is not covered by the backward compatibility promise
 */
class Quota
{
    const KEY_GLOBAL = "qg";
    const KEY_REMOTEADDR_PREFIX = "qa";
    const HASH_ALGO = "sha1";

    /**
     * @var QuotaDriverInterface
     */
    protected $driver;

    /**
     * @var int
     */
    protected $globalQuota;

    /**
     * @var DateInterval
     */
    protected $globalLifetime;

    /**
     * @var int
     */
    protected $remoteAddrQuota;

    /**
     * @var DateInterval
     */
    protected $remoteAddrLifetime;

    /**
     * @param QuotaDriverInterface $driver
     *  The storage driver
     * @param int $globalQuota
     * @param int|DateInterval|string $globalLifetime,
     * @param int $remoteAddrQuota
     * @param int|DateInterval|string $remoteAddrLifetime,
     */
    public function __construct(
        QuotaDriverInterface $driver,
        $globalQuota,
        $globalLifetime,
        $remoteAddrQuota,
        $remoteAddrLifetime
    ) {
        $this->driver = $driver;
        $this->globalQuota = (int) $globalQuota;
        $this->globalLifetime = $this->getTtl($globalLifetime);
        $this->remoteAddrQuota = $remoteAddrQuota;
        $this->remoteAddrLifetime = $this->getTtl($remoteAddrLifetime);
    }

    /**
     * Clean up expired entries
     *
     * Forwards the call to the driver if an "evict" method exists, so that the driver can take all actions required
     * to make the underlying implementations get rid of obsolete entries.
     */
    public function evict(): void
    {
        if (\method_exists($this->driver, 'evict')) {
            $this->driver->evict();
        }
    }

    /**
     * Register a hit against global and individual quota
     *
     * @param string $addr
     *  The remote address
     *
     * @throws ServiceUnavailableException
     *  If the global quota is violated
     * @throws TooManyRequestsException
     *  If the individual quota is violated
     */
    public function hit(
        $addr
    ): void {
        if ($this->globalQuota > 0) {
            $value = $this->driver->hitQuota(static::KEY_GLOBAL, $this->globalLifetime);

            if ($value > $this->globalQuota) {
                throw new ServiceUnavailableException;
            }
        }

        if ($this->remoteAddrQuota > 0) {
            $key = static::KEY_REMOTEADDR_PREFIX . \hash(static::HASH_ALGO, $addr, true);
            $value = $this->driver->hitQuota($key, $this->remoteAddrLifetime);

            if ($value > $this->remoteAddrQuota) {
                throw new TooManyRequestsException;
            }
        }
    }

    /**
     * Initialize the quota
     *
     * Forwards the call to the driver if a "setup" method exists, so that the driver can take all actions required
     * to make the underlying implementations ready to go (like setting up database tables).
     */
    public function setup(): void
    {
        if (\method_exists($this->driver, 'setup')) {
            $this->driver->setup();
        }
    }

    protected function getTtl(
        $ttl
    ) {
        if (\is_numeric($ttl)) {
            return (int) $ttl;
        } elseif ($ttl instanceof DateInterval) {
            return $ttl;
        } elseif (\is_string($ttl) && $ttl[0] === "P") {
            return new DateInterval($ttl);
        } else {
            return 0;
        }
    }
}

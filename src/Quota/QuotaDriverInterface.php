<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\Quota;

use DateInterval;

interface QuotaDriverInterface
{
    /**
     * Count a hit against a quota
     *
     * @param string $name
     *  The identifier for the quota
     * @param int|DateInterval $ttl
     *  Duration for which the hit should have an impact
     * @param int $weight
     *  The weight of the hit
     *
     * @return int
     *  The value against quota after the hit
     */
    public function hitQuota(
        string $name,
        $ttl,
        int $weight = 1
    ): int;
}

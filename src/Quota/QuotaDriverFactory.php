<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\Quota;

use mysqli;

use nextdev\Geoffrey\Quota\MysqliQuotaDriver;
use nextdev\Geoffrey\Quota\NullQuotaDriver;
use nextdev\Geoffrey\Quota\QuotaDriverInterface;

class QuotaDriverFactory
{
    public static function createQuotaDriver(
        $client,
        $config
    ): QuotaDriverInterface {
        if ($client instanceof mysqli) {
            return new MysqliQuotaDriver($client, $config['table'] ?? "quota");
        } else {
            return new NullQuotaDriver();
        }
    }
}

<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\Quota;

use DateInterval;
use DateTimeImmutable;

use nextdev\Geoffrey\Driver\MysqliDriverAbstract;
use nextdev\Geoffrey\Quota\QuotaDriverInterface;

class MysqliQuotaDriver extends MysqliDriverAbstract implements QuotaDriverInterface
{
    /**
     * Clean up expired entries
     */
    public function evict(): void
    {
        $this->mysqli->query(\sprintf(
            "DELETE FROM %s WHERE %s < NOW()",
            $this->table($this->tableName),
            $this->field('expires')
        ));
    }

    /**
     * Count a hit against a quota
     *
     * @param string $name
     *  The identifier for the quota
     * @param int|DateInterval $ttl
     *  Duration for which the hit should have an impact
     * @param int $weight
     *  The weight of the hit
     *
     * @return int
     *  The value against quota after the hit
     */
    public function hitQuota(
        string $name,
        $ttl,
        int $weight = 1
    ): int {
        if ($ttl instanceof DateInterval) {
            $d0 = new DateTimeImmutable();
            $ttl = $d0->add($ttl)->getTimestamp() - $d0->getTimestamp();
        }
        $ttl = (int) $ttl;
        if ($ttl <= 0 || $weight <= 0) {
            return 0;
        }

        $q = $this->mysqli->multi_query($s = \implode("; ", [
            \sprintf(
                'INSERT %s SET %s = %s, %s = NOW(), %s = TIMESTAMPADD(SECOND, %d, NOW()), %s = %d '.
                    ' ON DUPLICATE KEY UPDATE '.
                    ' %7$s = %7$s * (1 - LEAST(TIMESTAMPDIFF(SECOND, %4$s, NOW()), %6$d) / %6$d) + %8$d'.
                    ', %4$s = NOW(), %5$s = TIMESTAMPADD(SECOND, %6$d, NOW())',
                $this->table($this->tableName),
                $this->field('key'),
                $this->value($name),
                $this->field('mtime'),
                $this->field('expires'),
                $this->value($ttl),
                $this->field('weight'),
                $this->value($weight)
            ),
            \sprintf(
                'SELECT %s FROM %s WHERE %s=%s',
                $this->field('weight'),
                $this->table($this->tableName),
                $this->field('key'),
                $this->value($name)
            ),
        ]));

        $result = 0;
        if ($q) {
            do {
                if ($r = $this->mysqli->store_result()) {
                    $result = $r->fetch_row()[0] ?? 0;
                }
            } while ($this->mysqli->more_results() && $this->mysqli->next_result());
        }

        return $result;
    }

    /**
     * Initialize the quota storage
     */
    public function setup(): void
    {
        $this->mysqli->query(\sprintf(
            "DROP TABLE IF EXISTS %s",
            "`" . $this->mysqli->real_escape_string($this->tableName) . "`"
        ));
        $this->mysqli->query(\sprintf(
            "CREATE TABLE IF NOT EXISTS %s (%s, %s, %s, %s, %s)",
            "`" . $this->mysqli->real_escape_string($this->tableName) . "`",
            "`key` VARBINARY(64)",
            "`mtime` TIMESTAMP",
            "`expires` TIMESTAMP",
            "`weight` FLOAT",
            "PRIMARY KEY (`key`)",
            "INDEX(`mtime`)"
        ));
    }
}

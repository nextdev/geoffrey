<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\Driver;

use DateInterval;
use DateTime;
use mysqli;

abstract class MysqliDriverAbstract
{
    /**
     * @var mysqli
     */
    protected $mysqli;

    /**
     * @var string
     */
    protected $tableName;

    public function __construct(
        mysqli $mysqli,
        string $tableName
    ) {
        $this->mysqli = $mysqli;
        $this->tableName = $tableName;
    }

    protected function expires(
        $ttl
    ): ?string {
        if (\is_int($ttl)) {
            $ttl = new DateInterval("PT" . $ttl . "S");
        }
        return ($ttl instanceof DateInterval)? (new DateTime)->add($ttl)->format("Y-m-d H:i:s"): null;
    }

    protected function field(
        string $name
    ): string {
        return "`" . $this->mysqli->real_escape_string($name) . "`";
    }

    protected function table(
        string $name
    ): string {
        return "`" . $this->mysqli->real_escape_string($name) . "`";
    }

    protected function value(
        string $value
    ): string {
        if (!isset($value)) {
            return "NULL";
        } elseif (\is_bool($value)) {
            return $value? "TRUE": "FALSE";
        } elseif (\is_numeric($value)) {
            return $value;
        }
        return "'" . $this->mysqli->real_escape_string($value) . "'";
    }
}

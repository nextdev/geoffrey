<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\Request;

use Psr\Http\Message\RequestInterface;

/**
 * Manipulate requests according to mapping rules
 *
 * @internal This class is not covered by the backward compatibility promise
 */
class Map
{
    /**
     * @var array
     */
    private $mappingRules;

    /**
     * Construct the URL Mapping
     *
     * @param array $mapping
     */
    public function __construct(
        array $map
    ) {
        \krsort($map);
        $this->mappingRules = $map;
    }

    /**
     * Map a request according to configuration
     *
     * @param RequestInterface $request
     *  The request to map
     * @param bool $required
     *  If true, return null if no mapping rule matches the requested URI
     *  Otherwise return the unaltered $request
     *
     * @return RequestInterface|null
     */
    public function mapRequest(
        RequestInterface $request,
        bool $required = true
    ): ?RequestInterface {
        $forwardedUrl = $this->getForwardedUrl($request);

        foreach ($this->mappingRules as $pattern => $target) {
            if (!$this->matchForwardedUrl($forwardedUrl, $pattern)) {
                continue;
            }
            $target = $this->harmonizeTarget($target);
            if (isset($target['url'])) {
                $request = $this->mapRequestUrl($request, $forwardedUrl, \strlen($pattern), $target['url']);
            }
            if (isset($target['server'])) {
                $request = $this->mapRequestServer($request, $target['server']);
            }

            return $request;
        }

        return $required? null: $request;
    }

    protected function getForwardedUrl(
        RequestInterface $request
    ): string {
        $uri = $request->getUri();

        $host = $uri->getHost();
        $port = $uri->getPort();
        $path = $uri->getPath();
        
        return $host . (isset($port)? ":" . $port : "") . ((isset($path[0]) && $path[0] !== "/")? "/": "") . $path;
    }

    protected function harmonizeTarget(
        $target
    ): array {
        if (\is_string($target)) {
            if ($target[0] === "[" || \preg_match("/^[\d.]+$/", $target)) {
                $target = ['server' => $target];
            } else {
                $target = ['url' => $target];
            }
        }

        return $target;
    }

    protected function mapRequestServer(
        RequestInterface $request,
        string $targetServer
    ): RequestInterface {
        $uri = $request->getUri();
        $host = $request->getHeader("Host")[0] ?? "";

        $parsedTarget = \parse_url("//" . $targetServer);
        foreach ($parsedTarget as $k => $v) {
            if ($k === 'host') {
                $uri = $uri->withHost($v);
            } elseif ($k === 'port') {
                $uri = $uri->withPort($v);
            }
        }

        $request = $request->withUri($uri);
        if ($host !== "") {
            $request = $request->withHeader("Host", $host);
        }

        return $request;
    }

    protected function mapRequestUrl(
        RequestInterface $request,
        string $forwardedUrl,
        int $patternLength,
        string $targetUrl
    ): RequestInterface {
        $uri = $request->getUri();
        $pathSegment = \substr($forwardedUrl, $patternLength);

        if (isset($targetUrl[0]) && $targetUrl[0] !== "/" && \strpos($targetUrl, ":/") === false) {
            $targetUrl = "//" . $targetUrl;
        }
        if (isset($targetUrl[-1]) && $targetUrl[-1] !== "/" && isset($pathSegment[0]) && $pathSegment[0] !== "/") {
            $pathSegment = "/" . $pathSegment;
        }

        $parsedTarget = \parse_url($targetUrl . $pathSegment);
        foreach ($parsedTarget as $k => $v) {
            if ($k === 'scheme') {
                $uri = $uri->withScheme($v);
            } elseif ($k === 'host') {
                $uri = $uri->withHost($v);
            } elseif ($k === 'port') {
                $uri = $uri->withPort($v);
            } elseif ($k === 'user') {
                $uri = $uri->withUserInfo($v, $parsedTarget['pass'] ?? null);
            } elseif ($k === 'path') {
                $uri = $uri->withPath($v);
            } elseif ($k === 'query' && $v !== "") {
                $uri = $uri->withQuery($v . (($uri->getQuery() !== "")? "&" . $uri->getQuery(): ""));
            }
        }
        $path = $parsedTarget['path'] ?? $pathSegment;
        $uri = $uri->withPath(($path === "")? "/": $path);

        return $request->withUri($uri);
    }

    protected function matchForwardedUrl(
        string $forwardedUrl,
        string $pattern
    ): bool {
        $l = \strlen($pattern);
        return \substr_compare($forwardedUrl, $pattern, 0, $l) === 0
            && (\strlen($forwardedUrl) === $l || $pattern[$l-1] === "/" || $forwardedUrl[$l] === "/");
    }
}

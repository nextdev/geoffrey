<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\Request;

use Psr\Http\Message\UriInterface;

class UriParser
{
    /**
     * Parse an URL relative to a BaseURI
     *
     * @param string $url
     * @param UriInterface $baseUri
     */
    public function parseUrl(
        string $url,
        UriInterface $baseUri
    ): UriInterface {
        $parsedUrl = \parse_url($url);

        if (isset($parsedUrl['scheme']) || isset($parsedUrl['host'])) {
            return new Uri(
                $parsedUrl['scheme'],
                $parsedUrl['host'],
                $parsedUrl['port'] ?? null,
                $parsedUrl['path'] ?? "/",
                $parsedUrl['query'] ?? "",
                $parsedUrl['fragment'] ?? "",
                $parsedUrl['user'] ?? "",
                $parsedUrl['pass'] ?? ""
            );
        }
        $uri = $baseUri->withFragment("");

        if (isset($parsedUrl['path'])) {
            if ($parsedUrl['path'][0] === "/") {
                $uri = $uri->withPath($parsedUrl['path']);
            } else {
                $path = \array_slice(\explode("/", $baseUri->getPath()), 1, -1);
                $path = \array_slice($path, 1, -1);
                $path = \array_filter($path, function ($v) {
                    return $v !== "";
                });
                foreach (\explode("/", $parsedUrl['path']) as $p) {
                    if ($p === "." || $p === "") {
                        continue;
                    } elseif ($p === "..") {
                        \array_pop($path);
                    } else {
                        $path[] = $p;
                    }
                }
                $uri = $uri->withPath("/" . \implode("/", $path));
            }
            $uri = $uri->withQuery("");
        }
        if (isset($parsedUrl['query'])) {
            $uri = $uri->withQuery($parsedUrl['query']);
        }
        $uri = $uri->withFragment($parsedUrl['fragment'] ?? "");
        if (isset($parsedUrl['user'])) {
            $uri = $uri->withUserInfo($parsedUrl['user'], $parsedUrl['pass'] ?? "");
        }

        return $uri;
    }
}

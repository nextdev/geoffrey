<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Thanks to TYPO3/phar-stream-wrapper
 */
namespace nextdev\Geoffrey\Request;

use LogicException;

/**
 * @internal This class is not covered by the backward compatibility promise
 * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
 */
class StreamInterceptor
{
    /**
     * Internal stream constants that are not exposed to PHP, but used...
     * @see https://github.com/php/php-src/blob/e17fc0d73c611ad0207cac8a4a01ded38251a7dc/main/php_streams.h
     */
    const STREAM_OPEN_FOR_INCLUDE = 128;

    const METADATA_METHODS = [
        \STREAM_META_ACCESS => 'chmod',
        \STREAM_META_GROUP => 'chgrp',
        \STREAM_META_GROUP_NAME => 'chgrp',
        \STREAM_META_OWNER => 'chown',
        \STREAM_META_OWNER_NAME => 'chown',
        \STREAM_META_TOUCH => 'touch',
    ];

    /**
     * @var array
     */
    protected static $callbacks;

    public static function register(
        callable $callback,
        string ...$protocol
    ): void {
        foreach ($protocol as $p) {
            self::$callbacks[$p] = $callback;
            self::doRegister($p);
        }
    }

    public static function restore(
        $protocols = null
    ): void {
        if (!isset($protocols)) {
            $protocols = \array_keys(self::$callbacks);
        }

        foreach ($protocols as $p) {
            self::doRestore($p);
            unset(self::$callbacks[$p]);
        }
    }

    protected static function doRegister(
        string $protocol
    ): void {
        if (!isset(self::$callbacks[$protocol])) {
            return;
        }
        \stream_wrapper_unregister($protocol);
        \stream_wrapper_register($protocol, static::class, \STREAM_IS_URL);
    }

    protected static function doRestore(
        string $protocol
    ): void {
        \stream_wrapper_restore($protocol);
    }

    /**
     * @var resource
     */
    public $context;

    /**
     * @var resource
     */
    public $handle;

    /**
     * @var string
     */
    public $protocol;

    public function __construct()
    {
    }

    public function dir_closedir(): bool
    {
        if (!isset($this->handle)) {
            return false;
        }

        $this->invoke('closedir', $this->handle);

        return isset($this->handle) === false;
    }

    public function dir_opendir(
        string $path,
        int $options
    ): bool {
        $this->setProtocol($path);

        $res = $this->invoke('opendir', $this->handle);

        if (\is_resource($res)) {
            $this->handle = $res;
            return true;
        }
        return false;
    }

    public function dir_readdir(): string
    {
        return $this->invoke('readdir', $this->handle);
    }

    public function dir_rewinddir(): bool
    {
        if (!isset($this->handle)) {
            return false;
        }

        $this->invoke('rewinddir', $this->handle);

        return isset($this->handle);
    }

    public function mkdir(
        string $path,
        int $mode,
        int $options
    ): bool {
        $this->setProtocol($path);

        return $this->invoke('mkdir', $path, $mode, (bool)($options & \STREAM_MKDIR_RECURSIVE), $this->context);
    }

    public function rename(
        string $path,
        string $pathNew
    ): bool {
        $this->setProtocol($path);

        return $this->invoke('rename', $path, $pathNew, $this->context);
    }

    public function rmdir(
        string $path,
        int $options
    ): bool {
        $this->setProtocol($path);

        return $this->invoke('rmdir', $path, $this->context);
    }

    /*
    public function stream_cast(
        int $castAs
    ): resource {
        throw new LogicException(\sprintf("%s not implemented", __METHOD__));
    }
    */

    public function stream_close(): void
    {
        $this->invoke('fclose', $this->handle);
    }

    public function stream_eof(): bool
    {
        return $this->invoke('feof', $this->handle);
    }

    public function stream_flush(): bool
    {
        return $this->invoke('fflush', $this->handle);
    }

    public function stream_flock(
        int $mode
    ): bool {
        return $this->invoke('flock', $this->handle, $mode);
    }

    public function stream_metadata(
        string $path,
        int $option,
        $value
    ): bool {
        $this->setProtocol($path);

        return isset(self::METADATA_METHODS[$option])?
            $this->invoke(self::METADATA_METHODS[$option], $path, $value):
            false;
    }

    public function stream_open(
        string $path,
        string $mode,
        int $options,
        ?string &$opened_path
    ): bool {
        $this->setProtocol($path);

        if ($options & self::STREAM_OPEN_FOR_INCLUDE) {
            if (function_exists('opcache_reset')
                && function_exists('opcache_get_status')
                && !empty(opcache_get_status()['opcache_enabled'])
            ) {
                opcache_reset();
            }
            $h = $this->invoke('fopen', $path, $mode, (bool)($options & \STREAM_USE_PATH));
        } else {
            $h = $this->invoke('fopen', $path, $mode, (bool)($options & \STREAM_USE_PATH), $this->context);
        }

        if (!\is_resource($h)) {
            return false;
        }
    
        if ($options & \STREAM_USE_PATH) {
            $opened_path = \stream_get_meta_data($h)['uri'];
        }

        $this->handle = $h;
        return true;
    }

    public function stream_read(
        int $count
    ): string {
        return $this->invoke('fread', $this->handle, $count);
    }

    public function stream_seek(
        int $offset,
        int $whence = \SEEK_SET
    ): bool {
        return $this->invoke('fseek', $this->handle, $offset, $whence) === 0;
    }

    public function stream_set_option(
        int $option,
        int $arg1,
        int $arg2
    ): bool {
        switch ($option) {
            case \STREAM_OPTION_BLOCKING:
                return $this->invoke('stream_set_blocking', $this->handle, $arg1);
            case \STREAM_OPTION_READ_BUFFER:
                return $this->invoke('stream_set_read_buffer', $this->handle, $arg2);
            case \STREAM_OPTION_READ_TIMEOUT:
                return $this->invoke('stream_set_timeout', $this->handle, $arg1, $arg2);
            case \STREAM_OPTION_WRITE_BUFFER:
                return $this->invoke('stream_set_write_buffer', $this->handle, $arg2);
        }
        return false;
    }

    public function stream_stat(): array
    {
        return $this->invoke('fstat', $this->handle);
    }

    public function stream_tell(): int
    {
        return $this->invoke('ftell', $this->handle);
    }

    public function stream_truncate(
        int $size
    ): bool {
        return $this->invoke('ftruncate', $this->handle, $size);
    }

    public function stream_write(
        string $data
    ): int {
        return $this->invoke('fwrite', $this->handle, $data);
    }

    public function unlink(
        string $path
    ): bool {
        $this->setProtocol($path);

        return $this->invoke('unlink', $path, $this->context);
    }

    public function url_stat(
        string $path,
        int $flags
    ): array {
        $this->setProtocol($path);

        $prefix = (($flags & \STREAM_URL_STAT_QUIET)? "@": "") . (($flags & \STREAM_URL_STAT_LINK)? "l": "");

        return $this->stat(
            $this->invoke($prefix . "stat", $path)
        );
    }

    protected function setProtocol(
        string $url
    ) {
        $parsedUrl = \parse_url($url);

        // just for testing
        if (isset($this->protocol) && $this->protocol !== ($parsedUrl['scheme'] ?? "file")) {
            throw new LogicException("protocol change from $this->protocol to $url");
        }

        $this->protocol = $parsedUrl['scheme'] ?? "file";
    }

    protected function invoke(
        string $funcName,
        ...$args
    ) {
        if (!isset($this->protocol)) {
            throw new LogicException("should not be called before the protocol is determined");
        }

        $done = $this::$callbacks[$this->protocol]($this, $funcName, $args, $returnValue);

        if ($done === true) {
            return $returnValue;
        }

        self::doRestore($this->protocol);
        try {
            if ($funcName[0] === "@") {
                return @\call_user_func_array(\substr($funcName, 1), $args);
            } else {
                return \call_user_func_array($funcName, $args);
            }
        } finally {
            self::doRegister($this->protocol);
        }
    }

    protected function stat(
        $value
    ): array {
        return \is_array($value)? $value: [
        ];
    }
}

<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey;

use LogicException;
use RuntimeException;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use function GuzzleHttp\Psr7\str;
use function GuzzleHttp\Psr7\stream_for;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Client\ClientInterface;
use Psr\SimpleCache\CacheInterface;
use Slim\Http\Environment;
use Slim\Http\Request as ServerRequest;

use nextdev\Geoffrey\Cache\Cache;
use nextdev\Geoffrey\Cache\CacheDriverFactory;
use nextdev\Geoffrey\Client\ClientFactory;
use nextdev\Geoffrey\ContentProcessor\ContentProcessorInterface;
use nextdev\Geoffrey\ContentProcessor\HtmlContentFrameProcessor;
use nextdev\Geoffrey\ContentProcessor\XmlStylesheetProcessor;
use nextdev\Geoffrey\Quota\Quota;
use nextdev\Geoffrey\Quota\QuotaDriverFactory;
use nextdev\Geoffrey\Quota\QuotaDriverInterface;
use nextdev\Geoffrey\Quota\ServiceUnavailableException;
use nextdev\Geoffrey\Quota\TooManyRequestsException;
use nextdev\Geoffrey\Request\Map;
use nextdev\Geoffrey\Request\StreamInterceptor;

class Geoffrey implements
    ClientInterface,
    RequestFactoryInterface,
    ResponseFactoryInterface,
    StreamFactoryInterface
{
    /** Print request and response */
    const DEBUG_PRINTMESSAGE = 0b1;

    /**
     * @var array
     */
    public $clientFactories = [
        'curl' => [ClientFactory::class, 'createCurlClient'],
        'mysql' => [ClientFactory::class, 'createMysqliClient'],
    ];

    /**
     * @var array
     */
    public $configProcessorsShortcuts = [
        'htmlContentFrame' => [
            'contentType' => [
                "text/html",
            ],
            'factory' => [HtmlContentFrameProcessor::class, 'createHtmlContentFrameProcessor'],
        ],
        'xmlStylesheet' => [
            'contentType' => [
                "application/xml",
            ],
            'factory' => [XmlStylesheetProcessor::class, 'createXmlStylesheetProcessor']
        ],
    ];

    /**
     * @var int
     */
    public $debug = 0;

    /**
     * @var array
     */
    public $driverFactories = [
        CacheInterface::class => [CacheDriverFactory::class, 'createCacheDriver'],
        QuotaDriverInterface::class => [QuotaDriverFactory::class, 'createQuotaDriver'],
    ];

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @var array
     */
    private $config;

    /**
     * @var array
     */
    private $clients = [];

    /**
     * @var string[]
     */
    private $interceptors = [];

    /**
     * @var Map
     */
    private $map;

    /**
     * @var Quota
     */
    private $quota;

    /**
     * @param array $config
     */
    public function __construct(
        array $config = []
    ) {
        $this->config = $config;
    }

    /**
     * Create a request
     *
     * @param string $method
     *  HTTP method
     * @param UriInterface|string $uri
     *  Request URI
     * @param array $headers
     *  Message headers
     * @param string|null|resource|StreamInterface $body
     *  Message body
     *
     * @return Request
     */
    public function createRequest(
        string $method,
        $uri,
        array $headers = [],
        $body = null,
        string $version = "1.1"
    ): RequestInterface {
        return new Request(
            $method,
            $uri,
            $headers,
            $body,
            $version
        );
    }

    /**
     * Create a response
     *
     * @param int $statusCode
     *  Status code
     *  default = 200 OK
     * @param string $reasonPhrase
     *  Reason phrase
     *  If empty the HTTP default will be used
     * @param array $headers
     *  Message headers
     * @param string|null|resource|StreamInterface $body
     *  Message body
     *
     * @return Response
     */
    public function createResponse(
        int $statusCode = 200,
        string $reasonPhrase = "",
        array $headers = [],
        $body = null,
        string $version = "1.1"
    ): ResponseInterface {
        return new Response(
            $statusCode,
            $headers,
            $body,
            $version,
            $reasonPhrase
        );
    }

    /**
     * Create a stream
     *
     * @param string $content
     *  The content for the stream
     *
     * @return StreamInterface
     */
    public function createStream(
        string $content = ""
    ): StreamInterface {
        return stream_for($content);
    }

    /**
     * Create a stream from file content
     *
     * @param string $path
     *  Path to the file
     * @param string $mode
     *  Mode for the underlying stream resource
     *
     * @return StreamInterface
     */
    public function createStreamFromFile(
        string $path,
        string $mode = "rb"
    ): StreamInterface {
        return stream_for(\fopen($path, $mode));
    }

    /**
     * Create a stream from an existing resource
     *
     * @param resource $resource
     *
     * @return StreamInterface
     */
    public function createStreamFromResource(
        $resource
    ): StreamInterface {
        return stream_for($resource);
    }

    /**
     * Clean up expired entries
     *
     * Forwards the call to cache and quota implementation
     */
    public function evict(): void
    {
        $this->getCache()->evict();
        $this->getQuota()->evict();
    }

    /**
     * Get config entries
     *
     * @param string ...$keys
     *  getConfig('a','b','c') will return $config['a']['b']['c'] or null if not set
     */
    public function getConfig(
        string ...$keys
    ) {
        $c =& $this->config;

        foreach ($keys as $k) {
            if (!isset($c[$k])) {
                return null;
            }
            $c =& $c[$k];
        }

        return $c;
    }

    public function getMap(): Map
    {
        if (!isset($this->map)) {
            $this->map = new Map($this->getConfig('map') ?? []);
        }

        return $this->map;
    }

    /**
     * Let Geoffrey fetch a resource
     *
     * @param RequestInterface $request
     * @param $strictMapping
     *  If true, for requested URIs that have no matching entry in $config['map'] return 400 Bad Response.
     *  Defaults to $config['scriptMapping'] or false.
     *
     * @return ResponseInterface
     */
    public function sendRequest(
        RequestInterface $request,
        bool $strictMapping = null
    ): ResponseInterface {
        $mappedRequest = $this->getMap()->mapRequest($request);
        if (isset($mappedRequest)) {
            $request = $mappedRequest;
        } elseif ($strictMapping ?? $this->getConfig('strictMapping') ?? false) {
            return new Response(400);
        }

        $this->teardownInterceptor();

        $response = $this->getCache()->sendRequest($request);

        $this->setupInterceptor();

        return $response;
    }

    /**
     * Forward the response to matching processors and return their result
     *
     * If a content processor throws a RuntimeException (e.g. for lacking resources) responds with 502 Bad Gateway.
     *
     * @param ResponseInterface $response
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     */
    public function process(
        ResponseInterface $response,
        RequestInterface $request
    ): ResponseInterface {
        $processors = $this->getConfig('processors') ?? [];

        foreach ($this->configProcessorsShortcuts as $shortcut => $a) {
            if (!is_array($a['contentType']) || !\is_callable($a['factory'])) {
                continue;
            }
            if (isset($processors[$shortcut]) && \is_array($processors[$shortcut])) {
                $processors[$shortcut]['factory'] = $a['factory'];
                foreach ($a['contentType'] as $c) {
                    $processors[$c][] = $processors[$shortcut];
                }
            }
        }

        while (true) {
            $contentType = \explode(";", $response->getHeader("Content-Type")[0] ?? "")[0] ?? null;
            foreach ($processors[$contentType] ?? [] as $i => &$p) {
                if (\is_string($p)) {
                    $p = ['factory' => $p];
                }
                if (\is_array($p) && isset($p['factory'])) {
                    if (\is_string($p['factory'])) {
                        if (\strpos($p['factory'], "::") !== false) {
                            $p['factory'] = \explode("::", $p['factory'], 2);
                        }
                    }
                    if (\is_callable($p['factory'])) {
                        $p = $p['factory']($this, $p);
                    }
                }
                if (($p instanceof ContentProcessorInterface) && $p->canHandle($contentType)) {
                    try {
                        $response = $p->process($response, $request, $this);
                    } catch (RuntimeException $e) {
                        return new Response(502);
                    }
                    unset($processors[$contentType][$i]);
                    $response->getBody()->rewind();
                    continue 2;
                }
            }
            break;
        }

        return $response;
    }

    /**
     * Let Geoffrey handle the request found in the environment
     *
     * @param array $env
     *  Environment Properties
     *  Defaults to $_SERVER
     *  @see https://www.slimframework.com/docs/v3/cookbook/environment.html
     */
    public function run(
        array $env = null
    ) {
        $request = $this->createRequestFromEnvironment($env);

        try {
            $this->getQuota()->hit($env['REMOTE_ADDR'] ?? $_SERVER['REMOTE_ADDR'] ?? "");

            $response = $this->sendRequest($request, $this->getConfig('strictMapping') ?? true);

            $response = $this->process($response, $request);
        } catch (ServiceUnavailableException $e) {
            $response = $this->createResponse(503);
        } catch (TooManyRequestsException $e) {
            $response = $this->createResponse(429);
        }

        if ($this->debug & self::DEBUG_PRINTMESSAGE) {
            \header("HTTP/1.1 503 Service Unavailable");
            \header("Content-Type: text/plain");
            \printf("%s\n\n%s\n\n%s", str($request), \str_pad("", 50, "-"), str($response));
            return;
        }

        $this->outputResponse($response);
    }

    /**
     * Intitialize underlying storages
     *
     * Forwards the call to cache and quota implementation
     */
    public function setup(): void
    {
        $this->getCache()->setup();
        $this->getQuota()->setup();
    }

    protected function createRequestFromEnvironment(
        array $env = null
    ): ServerRequest {
        if (!isset($env)) {
            $env = $_SERVER;
        }
        if (\php_sapi_name() === 'cli-server') {
            $env['SCRIPT_NAME'] = $env['SCRIPT_FILENAME'];
        }
        $env = new Environment($env);

        $request = ServerRequest::createFromEnvironment($env);
        if ($request->getMethod() === "") {
            $request = $request->withMethod($request->getOriginalMethod());
        }

        return $request;
    }

    protected function getCache(): Cache
    {
        if (!isset($this->cache)) {
            $this->cache = new Cache(
                $this->getDriver(CacheInterface::class, $this->getConfig('cache')),
                $this->getHttpClient(),
                $this,
                $this
            );
        }

        return $this->cache;
    }

    protected function getClient(
        string $name
    ): object {
        if (isset($this->clients[$name])) {
            return $this->clients[$name];
        }
    
        $c = $this->getConfig('client', $name);
        if (\is_object($c)) {
            return $c;
        }

        if (\is_string($c)) {
            $parsedUrl = \parse_url($c);
            $c = [
                'type' => $parsedUrl['scheme'],
                'host' => $parsedUrl['host'],
                'port' => $parsedUrl['port'],
                'user' => $parsedUrl['user'],
                'pass' => $parsedUrl['pass'],
                'path' => $parsedUrl['path'],
                'query' => $parsedUrl['query'],
                'fragment' => $parsedUrl['fragment'],
            ];
        }

        $type = $c['type'] ?? $name;
        if (!isset($this->clientFactories[$type]) || !\is_callable($this->clientFactories[$type])) {
            throw new LogicException(\sprintf(
                "Client factory for type '%s' is missing",
                $type
            ));
        }

        $client = $this->clientFactories[$type]($this, $c);

        if (!\is_object($client)) {
            throw new LogicException(\sprintf(
                "Client factory must return an object",
                $client
            ));
        }
        return $client;
    }

    protected function getDriver(
        string $interface,
        $config
    ): object {
        if ($config instanceof $interface) {
            return $config;
        }

        if (\is_string($config)) {
            $config = ['client' => $config];
        }
        if (isset($config['client']) && \is_string($config['client'])) {
            $config['client'] = $this->getClient($config['client']);
        }
        if ($config['client'] instanceof $interface) {
            return $config['client'];
        }
        if (!isset($this->driverFactories[$interface]) || !\is_callable($this->driverFactories[$interface])) {
            throw new LogicException(\sprintf(
                "Driver factory for interface '%s' is missing",
                $interface
            ));
        }

        $driver = $this->driverFactories[$interface]($config['client'] ?? null, $config);

        if (!($driver instanceof $interface)) {
            throw new LogicException(\sprintf(
                "Driver factory must return implementation of interface '%s'",
                $interface
            ));
        }
        return $driver;
    }

    protected function getHttpClient(): ClientInterface
    {
        if (!isset($this->httpClient)) {
            $this->httpClient = $this->getDriver(ClientInterface::class, $this->getConfig('http') ?? "curl");
        }

        return $this->httpClient;
    }

    protected function getQuota(): Quota
    {
        if (!isset($this->quota)) {
            $c = $this->getConfig('quota');
            $this->quota = new Quota(
                $this->getDriver(QuotaDriverInterface::class, $c),
                $c['global']['quota'] ?? 0,
                $c['global']['lifetime'] ?? 60,
                $c['remoteAddr']['quota'] ?? 0,
                $c['remoteAddr']['lifetime'] ?? 10
            );
        }

        return $this->quota;
    }

    protected function outputResponse(
        ResponseInterface $response
    ): void {
        if (\headers_sent()) {
            throw new RuntimeException("Headers have been sent prematurely");
        }

        \header_remove();
        foreach ($response->getHeaders() as $name => $a) {
            if (\strtolower($name) === "transfer-encoding") {
                continue;
            }
            foreach ($a as $value) {
                \header(\sprintf("%s: %s", $name, $value), false);
            }
        }

        \header(\sprintf(
            'HTTP/%s %s %s',
            $response->getProtocolVersion(),
            $response->getStatusCode(),
            $response->getReasonPhrase()
        ), true);

        \fpassthru($response->getBody()->detach());
    }

    protected function setupInterceptor(
        array $protocols = ["http", "https"]
    ): void {
        StreamInterceptor::register(function (
            StreamInterceptor $interceptor,
            string &$funcName,
            array &$args,
            &$returnValue
        ) {
            if ($funcName === "fopen") {
                $req = $this->createRequest("GET", $args[0]);
                $res = $this->sendRequest($req);
                $returnValue = ($res->getStatusCode() === 200)? $res->getBody()->detach(): false;
                return true;
            }
        }, ...$protocols);

        foreach ($protocols as $p) {
            $this->interceptors[$p] = true;
        }
    }

    protected function teardownInterceptor(
        array $protocols = null
    ): void {
        if ($protocols === null) {
            $protocols = \array_keys($this->interceptors);
        }
        $protocols = \array_filter($protocols, function ($p) {
            return isset($this->interceptors[$p]) && $this->interceptors[$p] === true;
        });

        StreamInterceptor::restore($protocols);

        foreach ($protocols as $p) {
            $this->interceptors[$p] = false;
        }
    }
}

<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\ContentProcessor;

use DOMDocument;
use DOMElement;
use RuntimeException;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;

use nextdev\Geoffrey\Geoffrey;
use nextdev\Geoffrey\ContentProcessor\ContentProcessorInterface;

/**
 * Embeds HTML resources in a framing HTML document
 */
class HtmlContentFrameProcessor implements ContentProcessorInterface
{
    public static function createHtmlContentFrameProcessor(
        Geoffrey $geoffrey,
        array $config = []
    ): self {
        return new static(
            $config,
            $geoffrey,
            $geoffrey,
            $geoffrey
        );
    }

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var RequestFactoryInterface
     */
    protected $requestFactory;

    /**
     * @var StreamFactoryInterface
     */
    protected $streamFactory;

    public function __construct(
        array $config,
        ClientInterface $client,
        RequestFactoryInterface $requestFactory,
        StreamFactoryInterface $streamFactory
    ) {
        $this->config = $config;
        $this->client = $client;
        $this->requestFactory = $requestFactory;
        $this->streamFactory = $streamFactory;
    }

    /**
     * @inheritDoc
     */
    public function canHandle(
        string $contentType
    ): bool {
        return \in_array($contentType, ["text/html"]);
    }

    /**
     * @inheritDoc
     */
    public function process(
        ResponseInterface $response,
        RequestInterface $request
    ): ResponseInterface {
        $contentDom = new DOMDocument;
        if (@$contentDom->loadHTML($response->getBody()->getContents()) === false) {
            throw new RuntimeException;
        }

        $header = $this->fetchRemoteStrings($this->config['header'] ?? null);
        $footer = $this->fetchRemoteStrings($this->config['footer'] ?? null);

        $nodeId = \uniqid();

        $htmlDom = new DOMDocument;
        if (@$htmlDom->loadHTML($header . "<div id='$nodeId'></div>" . $footer) === false) {
            throw new RuntimeException;
        }

        if (isset($contentDom->documentElement)) {
            foreach ($contentDom->documentElement->childNodes as $n) {
                if ($n->nodeType !== \XML_ELEMENT_NODE) {
                    continue;
                }
                $name = \strtolower($n->nodeName);
                if ($name === "head") {
                    $headNode = $htmlDom->getElementsByTagName("head")[0] ??
                        $htmlDom->documentElement->appendChild($htmlDom->createElement("head"));
                    $this->copyChildren($n, $headNode);
                } elseif ($name === "body") {
                    $contentNode = $htmlDom->getElementById($nodeId);
                    $this->copyChildren($n, $contentNode, false);
                }
            }
        }

        return $response->withBody($this->streamFactory->createStream($htmlDom->saveHTML()));
    }

    protected function copyChildren(
        DOMElement $sourceNode,
        DOMElement $targetNode,
        bool $copyAttributes = true
    ): void {
        if ($copyAttributes) {
            foreach ($sourceNode->attributes as $n) {
                $n = $targetNode->ownerDocument->importNode($n, true);
                $targetNode->setAttributeNode($n);
            }
        }
        foreach ($sourceNode->childNodes as $n) {
            $n = $targetNode->ownerDocument->importNode($n, true);
            $targetNode->appendChild($n);
        }
    }

    protected function fetchRemoteStrings(
        ?string $url
    ): ?string {
        if (!isset($url)) {
            return null;
        }

        $request = $this->requestFactory->createRequest("GET", $url);
        $response = $this->client->sendRequest($request);

        $status = $response->getStatusCode();
        if ($status < 200 || $status >= 300) {
            if ($status === 502) {
                \trigger_error($response->getBody()->getContents(), \E_USER_NOTICE);
            }
            return "";
        }

        return $response->getBody()->getContents();
    }

    protected function prefixCssRules(
        string $prefix,
        string $cssCode
    ): string {
        return $cssCode;
    }
}

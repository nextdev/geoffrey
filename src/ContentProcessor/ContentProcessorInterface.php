<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\ContentProcessor;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface ContentProcessorInterface
{
    /**
     * Check if the Processor handles this type of content
     */
    public function canHandle(
        string $contentType
    ): bool;

    /**
     * Process the given response
     *
     * @param ResponseInterface $response
     *  The response to process
     * @param RequestInterface $request
     *  The request that the response is associated with
     */
    public function process(
        ResponseInterface $response,
        RequestInterface $request
    ): ResponseInterface;
}

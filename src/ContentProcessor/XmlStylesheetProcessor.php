<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\ContentProcessor;

use DOMDocument;
use RuntimeException;
use XSLTProcessor;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;

use nextdev\Geoffrey\Geoffrey;
use nextdev\Geoffrey\ContentProcessor\ContentProcessorInterface;
use nextdev\Geoffrey\Request\UriParser;

/**
 * Converts XML resources according embedded xml-stylesheet processing instructions
 */
class XmlStylesheetProcessor implements ContentProcessorInterface
{
    public static function createXmlStylesheetProcessor(
        Geoffrey $geoffrey
    ): self {
        return new static(
            $geoffrey,
            $geoffrey,
            $geoffrey
        );
    }

    /**
     * @var string[]
     */
    public $acceptedXMLTypes = [
        "application/xml",
        "text/xml",
    ];

    /**
     * @var string[]
     */
    public $acceptedXSLTypes = [
        "application/xml",
        "application/xslt+xml",
        "text/xml",
        "text/xsl",
    ];

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var RequestFactoryInterface
     */
    protected $requestFactory;

    /**
     * @var StreamFactoryInterface
     */
    protected $streamFactory;

    public function __construct(
        ClientInterface $client,
        RequestFactoryInterface $requestFactory,
        StreamFactoryInterface $streamFactory
    ) {
        $this->client = $client;
        $this->requestFactory = $requestFactory;
        $this->streamFactory = $streamFactory;
    }

    /**
     * @inheritDoc
     */
    public function canHandle(
        string $contentType
    ): bool {
        return \in_array($contentType, $this->acceptedXMLTypes);
    }

    /**
     * @inheritDoc
     */
    public function process(
        ResponseInterface $response,
        RequestInterface $request
    ): ResponseInterface {
        $xmlDom = $this->createDomDocument($response, $request, $this->acceptedXMLTypes);
        foreach ($xmlDom->childNodes as $c) {
            if ($c->nodeType === \XML_ELEMENT_NODE) {
                return $response;
            } elseif ($c->nodeType === \XML_PI_NODE && $c->target === "xml-stylesheet") {
                $piAttr = $this->parseXmlStylesheetPi($c->data);
                if ($piAttr['type'] === "text/xsl" && isset($piAttr['href']) && $piAttr['href'] !== "") {
                    $href = ($uriParser ?? new UriParser)->parseUrl($piAttr['href'], $request->getUri());

                    $xslRequest = $this->requestFactory->createRequest("GET", $href);
                    $xslResponse = $this->client->sendRequest($xslRequest);
                    $xslDom = $this->createDomDocument($xslResponse, $xslRequest, $this->acceptedXSLTypes);

                    $xsltProcessor = new XSLTProcessor();
                    $xsltProcessor->importStylesheet($xslDom);

                    $xslNs = "http://www.w3.org/1999/XSL/Transform";
                    $xslOutput = $xslDom->getElementsByTagNameNS($xslNs, "output")[0] ?? null;
                    if (isset($xslOutput)) {
                        $outMethod = $xslOutput->getAttribute('method');
                        $outType = $xslOutput->getAttribute('media-type');
                        $outEncoding = $xslOutput->getAttribute('encoding');
                    } else {
                        $outMethod = $outType = $outEncoding = "";
                    }
                    if ($outType === "") {
                        if ($outMethod === "html") {
                            $outType = "text/html";
                        } elseif ($outMethod === "text") {
                            $outType = "text/plain";
                        } elseif ($outMethod === "xml" || $outMethod === "") {
                            $outType = "text/xml";
                        }
                    }

                    $result = $xsltProcessor->transformToXml($xmlDom);

                    return $response
                        ->withHeader("Content-Type", isset($outType)? $outType . ";" . $outEncoding: "")
                        ->withBody($this->streamFactory->createStream($result));
                }
            }
        }

        return $response;
    }

    protected function createDomDocument(
        ResponseInterface $response,
        RequestInterface $request,
        ?array $acceptedTypes = null
    ): DOMDocument {
        $contentType = \explode(";", $response->getHeader("Content-Type")[0] ?? "", 2)[0];

        if (is_array($acceptedTypes) && !\in_array($contentType, $acceptedTypes)) {
            throw new RuntimeException;
        }

        $xmlDom = new DOMDocument();
        if (@$xmlDom->loadXML($response->getBody()->getContents()) === false) {
            throw new RuntimeException;
        }
        $xmlDom->documentURI = (string) $request->getUri();

        return $xmlDom;
    }

    protected function parseXmlStylesheetPi(
        string $pi
    ): array {
        $attr = [];
        $n = \preg_match_all("#(?<=^|\\s) (\\w+) = (['\\\"]) (.*?) \\2 (?=\\s|\\b)#x", $pi, $matches, \PREG_SET_ORDER);
        if ($n > 0) {
            foreach ($matches as $set) {
                $attr[$set[1]] = $set[3];
            }
        }
        return $attr;
    }
}

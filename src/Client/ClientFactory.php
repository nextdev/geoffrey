<?php
/*
 * (c) Philipp Fritsche <ph.fritsche@nextdev.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace nextdev\Geoffrey\Client;

use mysqli;
use RuntimeException;

use Http\Client\Curl\Client;

use nextdev\Geoffrey\Geoffrey;

class ClientFactory
{
    public static function createCurlClient(
        Geoffrey $geoffrey,
        $config
    ): Client {
        return new Client(
            $geoffrey,
            $geoffrey,
            \is_array($config['options'] ?? null)? static::resolveCurlOptions($config['options']): []
        );
    }

    public static function createMysqliClient(
        Geoffrey $geoffrey,
        $config
    ): mysqli {
        if (!isset($config['dbname']) && isset($config['path'])) {
            $p = \ltrim($config['path'], "/");
            $config['dbname'] = \substr($p, 0, \strpos($p, "/")?: null);
        }
        if ($config['dbname'] == "") {
            $config['dbname'] = "Geoffrey";
        }

        @$m = new mysqli(
            (string) ($config['host'] ?? "127.0.0.1"),
            (string) ($config['user'] ?? "root"),
            (string) ($config['pass'] ?? ""),
            (string) $config['dbname'],
            (int) ($config['port'] ?? 3306)
        );

        if ($m->connect_errno !== 0) {
            throw new RuntimeException($m->connect_error);
        }

        return $m;
    }

    protected static function resolveCurlOptions(
        array $options
    ): array {
        foreach ($options as $k => $v) {
            if (\is_string($k)) {
                if (\substr($k, 0, 8) === "CURLOPT_" && \defined($k)) {
                    $options[\constant($k)] = $v;
                }
                unset($options[$k]);
            }
        }
        return $options;
    }
}

# Geoffrey

A small reverse proxy solution for providing (limited) access to (local) (legacy) services

## Installation

As easy as it gets: Use Composer!


## Usage

Serve `./www/index.php` by configuring your web server.
Set environment variable `GEOFFREY_CONFIG` to the location of the `config.json`. (Defaults to `./.etc/config.json`)

### Address Mapping

#### Rewrites request URIs and fetches the resource from there:
```json
{
    "map": {
        "frontdoor.example.com": "local.example.com/page123",
        "frontdoor2.example.com": {
            "url": "local.example.com/page456"
        }
    }
}
```

#### Fowards requests to destinations the client can not reach itself:
```json
{
    "map": {
        "frontdoor.example.com": "192.168.0.2:8001",
        "frontdoor2.example.com": {
            "server": "192.168.1.2"
        }
    }
}
```

### Clients for outbound connections

#### Define client options:
```json
{
    "client": {
        "mariadbConnection123": {
            "type": "mysql",
            "host": "127.0.0.1",
            "port": "3306",
            "user": "exampleUser",
            "pass": "pa$$word",
            "dbname": "Geoffrey"
        },
        "mariadbConnection456": "mysql://exampleUser:pa$$word@127.0.0.1:3306/Geoffrey"
    }
}
```

#### And use them for caching responses:
```json
{
    "cache": "mariadbConnection123"
}
```

#### Reconfigure the underlying cUrl-Client:
```json
{
    "client": {
        "curl": {
            "type": "curl",
            "options": {
                "CURLOPT_CONNECTTIMEOUT": 5
            }
        }
    }
}
```

### Response processing

#### Convert XML-Resources according to XSLT processing instructions it holds:
```json
{
    "processors": {
        "xmlStylesheet": {}
    }
}
```

#### Wrap body content of HTML-Pages in some header and footer:
```json
{
    "processors": {
        "htmlContentFrame": {
            "header": "cms.example.com/?get=header",
            "footer": "cms.example.com/?get=footer"
        }
    }
}
```
The concatenation of header and footer should result in a valid HTML document.

#### Or use own code:
```json
{
    "processors": {
        "text/x-owndocument": {
            "factory": "vendorABC\\NamespaceXYZ\\ExampleProcessor::createMe",
            "options": {
                "foo": "bar"
            }
        }
    }
}
```
The factory will receive the Geoffrey instance as first and the config entry as second argument.

### Don't let annoying guests annoy the masteries

#### Tell them to stop knocking on the door (429):
```json
{
    "quota": {
        "client": "mariaDBClient123",
        "remoteAddr": {
            "quota": 10,
            "lifetime": 10
        }
    }
}
```

#### Deny service (503) when there are too many guests:
```json
{
    "quota": {
        "client": "mariaDBClient123",
        "global": {
            "quota": 200,
            "lifetime": 60
        }
    }
}
```

Every request will count 1 against the quota.
The carryover weight of past requests will be `last_recorded_weight * duration_without_hit / lifetime`.

As in the example above: After 60 seconds without a hit there will be no carryover against the global quota of 200 from past requests.

### Clean up

Run `php ./script/evict.php` to make Cache and Quota get rid of obsolete data.

### Update / Reset

`php ./script/update.php` will be run after every `composer update`. Call it any other time to reset Cache and Quota.

### Debug

If environment variable `GEOFFREY_DEBUG` is set to `1`, Geoffrey will respond with a 503 status and print the effective (normalized) request it received and the response it produced.
